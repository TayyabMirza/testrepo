var mongoose            = require('mongoose');
var mongoosePaginate    = require('mongoose-paginate');

var Schema = mongoose.Schema;
var empSchema = new Schema({
    
    fname: String,
    lname: String,
    cnic: String,
    birthDate: Date
    
},{ collection: 'employee', timestamps: { createdAt: 'createdAt' }});

empSchema.plugin(mongoosePaginate);
module.exports = Employee = mongoose.model('employee', empSchema); 