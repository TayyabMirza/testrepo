var mongoose            = require('mongoose');
var mongoosePaginate    = require('mongoose-paginate');

var Schema = mongoose.Schema;
var postsSchema = new Schema({
    
    
    userId: { type:Number, unique:true },
    id:     { type:Number, unique:true },
    title:  { type:String, unique:true },
    body:   { type:String, unique:true }
    
},{ collection: 'posts', timestamps: { createdAt: 'createdAt' }});

postsSchema.plugin(mongoosePaginate);
module.exports = Posts = mongoose.model('posts', postsSchema); 

