var express         = require('express');
var employeeCtrl    = require('../controllers/employeeCtrl');
var postsCtrl       = require('../controllers/postsCtrl');

var adminRoute = express.Router();

adminRoute.route('/employee/all').post(employeeCtrl.getEmployees);      // FindAll
adminRoute.route('/employee/add').post(employeeCtrl.addEmployee);      // Add New
adminRoute.route('/employee/:id').get(employeeCtrl.getEmployeeById);    // FindById
adminRoute.route('/employee/edit/:id').put(employeeCtrl.putEmployee);   // UpdateOne
adminRoute.route('/employee/:id').delete(employeeCtrl.deleteEmployee);  // DeleteOne

adminRoute.route('/posts/add/all').get(postsCtrl.addPosts);             // Add posts to DB.
adminRoute.route('/post/all').post(postsCtrl.getPosts);                 // FindAll
adminRoute.route('/post/add').post(postsCtrl.addPost);                  // Add New
adminRoute.route('/post/:id').get(postsCtrl.getPostById);               // FindById
adminRoute.route('/post/edit/:id').put(postsCtrl.putPost);              // UpdateOne
adminRoute.route('/post/:id').delete(postsCtrl.deletePost);             // DeleteOne

module.exports = adminRoute;
