const express = require('express');
const app = express();
// const cors = require('cors');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const loggerService = require('expressjs-file-logger');
const loggerConfig = require('./config/logger');
const config = require('./config/database');
var jwt= require('jsonwebtoken'); 
var port = process.env.PORT || 3333;

// @logger service.
app.use(loggerService(loggerConfig));
// @database connection.
mongoose.connect(config.database, { useNewUrlParser: true, useCreateIndex: true, useFindAndModify: false, useUnifiedTopology:true })
const db = mongoose.connection;
db.once('open', function () { console.log('Database connection established!\n') });
db.on('error', function (err) { console.log(`Error: ${err}`) });


app.get('/', (req, res) => res.send('Hello World!'))

// @bodyParsing, urlEncoding, and cross origin access.
app.use(bodyParser.json({ limit: "100mb" }));
app.use(bodyParser.urlencoded({ limit: "100mb", extended: true }));
// app.use(cors());

// @Public static directory.
app.use(express.static(__dirname + '/public'));

// @Routes importing.
const adminRoutes = require('./routes/adminRoutes');

// @Routers initialization.
app.use('/admin', adminRoutes);

// @testing route.
app.get('/test', (req, res) => {
    res.json(jwt.sign({hash:'Abc123###'},'123456'));
})

app.post('/uploadfile', (req, res, next) => {
    console.log(req.body);
    const file = req.files;
    if (!file) {
        return res.json({
            error: true,
            status: 400,
            message: 'Please Upload JPG, JPEG or PNG File'
        });
    }
    res.json(JSON.parse(req.body));
})

app.listen(port, () => {
    console.log(`App Started at ${ Date() } on Port ${ port} `);
});
