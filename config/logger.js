const defaultOptions = {  
    "storagePath": "path.join(process.cwd(), '/logs')",   
    "logType": "hour",  
    "logNameSeparator": "-",  
    "logMode": "all",  
    "logRequestBody": "true",  
    "logFilesExtension": ".txt"  
 };  