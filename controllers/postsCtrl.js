var post = require('../schemas/postSchema');
var fetch = require('node-fetch');

module.exports = {

    addPosts: async function (req, res) {
        let data = [];
        await fetch('https://jsonplaceholder.typicode.com/posts')
            .then(res => res.json())    // convert buffer response to json.
            .then(json => data = json)  // put that json to data. just assigning.
        post.insertMany(data)
            .then(result => {
                res.status(200).json(result);
                console.log(result)
            })
            .catch(error => {
                res.status(403).json(error)
            })
    },

    getPosts: function(req, res){
        var page    = (req.body.page)?req.body.page:1;
        var perPage = (req.body.itemsPerPage)?req.body.itemsPerPage:10;
        var query   = {};
        var options = {
            lean:     true, 
            limit:    perPage,
            page:     page, 
        };
        post.paginate({},options)
            .then( result => { res.status(200).json(result) })
            .catch( error => { res.status(403).json(error) })
    },

    getPostById: function(req, res){
        post.findById(req.params.id)
            .then( result => { res.status(200).json(result) })
            .catch( error => { res.status(403).json(error) })
    },

    addPost: function(req, res){
        var Post = new post(req.body);
        Post.save()
        .then( result => { res.status(200).json(result) })
        .catch( error => { res.status(403).json(error) })
    },

    putPost: function(req, res){
        post.findByIdAndUpdate( req.params.id, req.body, {upsert:true, new:true})
            .then( result => { res.status(200).json(result) })
            .catch( error => { res.status(403).json(error) })
    },

    // deletePost: function(req, res){
    //     post.findByIdAndDelete(req.params.id)
    //         .then( result => { res.status(200).json('record deleted!') })
    //         .catch( error => { res.status(403).json(error) })
    // },

    deletePost: function(req, res){
        post.findById(req.params.id)
        .then( result => {
            if(result !== null){
                post.deleteOne({ _id:req.params.id })
                .then( result => { res.status(200).json('Record found and deleted!') })
                .catch( error => { res.status(403).json(error) })
            }
            else{
                res.status(403).json('No record found!');
            }
        })
    }
}