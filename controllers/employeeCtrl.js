var employee    = require('../schemas/employeeSchema');

module.exports = {

    getEmployees: function(req, res){
        employee.find({},( error, result)=> {
            if (error){ res.json(err.error)}
            else{ res.json(result)}
        })
    },

    getEmployeeById: function(req, res){
        employee.findById(req.params.id)
            .then( result => { res.status(200).json(result) })
            .catch( error => { res.status(403).json(error) })
    },

    // this is another way to call the api async.
    // getEmployeeById: async function(req, res){
    //     let data = await employee.findById({_id:req.param.id});
    //     res.status(200).json(data);
    // },

    addEmployee: function(req, res){
        var Employee = new employee(req.body);
        Employee.save()
            .then( result => { res.status(200).json(result); console.log(result) })
            .catch( error => { res.status(403).json(error) })  
    },

    
    putEmployee: function(req, res){
        employee.findByIdAndUpdate(req.params.id,req.body,{upsert:true})
            .then( result => { res.status(200).json(result) })
            .catch( error => { res.status(403).json(error) })
    },

    deleteEmployee: function(req, res){
        employee.deleteOne({_id: req.params.id})
            .then( result => { res.status(200).json('record deleted!') })
            .catch( error => { res.status(403).json(error) })
    },

    // getEmployees: function(req, res){
    //     var page = (req.body.page)?req.body.page:1;
    //     var perPage = (req.body.itemsPerPage)?req.body.itemsPerPage:10;
    //     var query   = {};
    //     var populatedDoc = ({path:'category',select:{'text':1}});
    //     var options = {
    //         populate: populatedDoc,
    //         lean:     true, 
    //         limit:    perPage,
    //         page:     page, 
    //         select:   {'photo.data':0} 
    //     };
    //     employee.paginate(query,options).then( function(result) {
    //         res.json(result);
    //     })
    // },
}